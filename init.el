(require 'package)
(package-initialize)

;; Add new achives to the list
(defvar my-archives
  '(("melpa-stable" . "https://stable.melpa.org/packages/")
    ("melpa" . "https://melpa.org/packages/")))

(dolist (archive my-archives)
  (add-to-list 'package-archives archive t))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("11e57648ab04915568e558b77541d0e94e69d09c9c54c06075938b6abc0189d8" default)))
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
    (doom-themes hlinum helm-rtags cmake-mode helm-projectile projectile cmake-ide flycheck-irony flycheck company-irony-c-headers company-irony helm magit git-gutter-fringe+ undo-tree company-rtags rtags molokai-theme flycheck-rtags))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;; HINT : to automatically install selected package use the following command :
;; M-x package-install-selected-packages


;;;;;;;;;;;;;;;;;;;; UI

;; HINT note : next buffer is C-x right arrow (XF86Forward), previous is C-x left (XF86Back)


;; doom themes
(require 'doom-themes)

;; Global settings (defaults)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled

;; Load the theme (doom-one, doom-molokai, etc); keep in mind that each
;; theme may have their own settings.
;; (load-theme 'doom-tomorrow-night t)
(load-theme 'doom-molokai t)

;; Enable flashing mode-line on errors
(doom-themes-visual-bell-config)


(if (display-graphic-p)
    (progn
      ;; if graphic

      ;; highlight current line
      (global-hl-line-mode))
  
  ;; else (optional)
  (load "molokai-theme.el"))


;; highlight current line number in linum-mode
(require 'hlinum)
(hlinum-activate)


;; remove tool and menu bars
(tool-bar-mode -1)
(menu-bar-mode -1)


;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time
;; (require 'smooth-scroll)
;; (smooth-scroll-mode t)


;;;;;;;;;;;;;;;;;;;;; GIT

(require 'git-gutter-fringe+)
(global-git-gutter+-mode)
(setq git-gutter-fr+-side 'right-fringe)


;;;;;;;;;;;;;;;;;;;;; CODE EDITION

(delete-selection-mode 1)
(electric-pair-mode 1)
(show-paren-mode 1)
(global-undo-tree-mode)

(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

;; remap C-a to `smarter-move-beginning-of-line'
(global-set-key [remap move-beginning-of-line]
                'smarter-move-beginning-of-line)




;;;;;;;;;;;;;;;;;;;;;; C++ specific stuff
;; default tab width is 8
;;(setq c-basic-offset 4)
(setq c-default-style "linux")
(add-hook 'c-mode-hook 'linum-mode)
(add-hook 'c++-mode-hook 'linum-mode)


;; debugging with gdb
(defun my-gdb-mode-bindings ()
  (local-set-key (kbd "<f12>") 'gdb-many-windows))

(add-hook 'gdb-mode-hook 'my-gdb-mode-bindings)



;; HINT : some plugins will need cmake and libclang installed

;; see http://syamajala.github.io/c-ide.html
;; install rtags https://github.com/Andersbakken/rtags#tldr-quickstart or from packages

;; HINT : Before using RTags you need to start rdm and index your project. In order to index your project, RTags requires you to export your project's compile commands with cmake.
;; $ rdm &
;; $ cd /path/to/project/root
;; $ cmake . -DCMAKE_EXPORT_COMPILE_COMMANDS=1
;; $ rc -J .

;; this is potentially automated by cmake-ide below


;; HINT : A note on company : you can see code documentation by pressing F1 while completion box is open

;; HINT : for completion provided by irony, the irony server must be installed :
;; run M-x irony-install-server

(require 'rtags)
(require 'company-rtags)

(setq rtags-completions-enabled t)
(eval-after-load 'company
  '(add-to-list
    'company-backends 'company-rtags))
(setq rtags-autostart-diagnostics t)
(rtags-enable-standard-keybindings)


(defun my-add-c-mode-bindings ()
  (local-set-key (kbd "<f12>") 'rtags-find-symbol-at-point)
  (local-set-key (kbd "<M-f12>") 'rtags-find-references-at-point)
  (local-set-key (kbd "C->") 'rtags-location-stack-back)
  (local-set-key (kbd "C-<") 'rtags-location-stack-forward))

(add-hook 'c-mode-hook 'my-add-c-mode-bindings)
(add-hook 'c++-mode-hook 'my-add-c-mode-bindings)

(setq rtags-display-result-backend 'helm)


;; source code completion via Irony
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
;; (add-hook 'objc-mode-hook 'irony-mode)

(defun my-irony-mode-hook ()
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))

(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

;; Company with Irony
(add-hook 'c++-mode-hook 'company-mode)
(add-hook 'c-mode-hook 'company-mode)
(add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)
(setq company-backends (delete 'company-semantic company-backends))

(setq company-idle-delay 0)
(define-key c-mode-map [(backtab)] 'company-complete)
(define-key c++-mode-map [(backtab)] 'company-complete)


;; header file completion
(require 'company-irony-c-headers)
(eval-after-load 'company
  '(add-to-list
    'company-backends '(company-irony-c-headers company-irony)))


;; syntax checking with flycheck
(add-hook 'c++-mode-hook 'flycheck-mode)
(add-hook 'c-mode-hook 'flycheck-mode)

(require 'flycheck-rtags)

(defun my-flycheck-rtags-setup ()
  (flycheck-select-checker 'rtags)
  (setq-local flycheck-highlighting-mode nil) ;; RTags creates more accurate overlays.
  (setq-local flycheck-check-syntax-automatically nil))
;; c-mode-common-hook is also called by c++-mode
(add-hook 'c-mode-common-hook #'my-flycheck-rtags-setup)


;; Flycheck with Irony
(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))


;; CMake automation ()
(cmake-ide-setup)
(global-set-key (kbd "<f5>") 'cmake-ide-compile)

;; HINT : To have cmake-ide automatically create a compilation commands file
;; in your project root create a .dir-locals.el containing the following:
;; ((nil . ((cmake-ide-build-dir . "<PATH_TO_PROJECT_BUILD_DIRECTORY>"))))


;; helm mode everywhere
(global-set-key (kbd "M-x") 'helm-M-x)
(helm-mode 1)


;; CMake syntax highlighting
(setq load-path (cons (expand-file-name "/dir/with/cmake-mode") load-path))
(require 'cmake-mode)
(add-hook 'cmake-mode-hook 'linum-mode)


;; project management with projectile
(global-set-key (kbd "C-x M-f") 'helm-projectile-find-file-dwim)
(global-set-key (kbd "C-x M-g") 'helm-projectile-grep)
