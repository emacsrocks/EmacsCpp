My custom emacs for C/C++
=========================

Initial config
--------------

Clone the repository using the following :
```bash
git clone git@gitlab.com:Babouchot/EmacsCpp.git .emacs.d
```

or 
```
git clone git@gitlab.com:Babouchot/EmacsCpp.git .emacs.d.cpp
``` 
and symlink .emacs.d to it when needed if you have several emacs configurations living in parallel.


run : 
```
M-x package-refresh-contents
M-x package-install-selected-packages
```

note : there might be some errors while runing the package installation liek "Lisp nesting exceeds 'max-lisp-eval-depth'. Just run the command several times until all packages are installed. Or force install packages manually.
(setq max-lisp-eval-depth 10000) might help.

close emacs and reopen to reload config.

Finalize C++ support by installing the irony server : 
```
M-x irony-install-server
```
You will need libclang-dev installed for this to succeed.

Project Config
--------------

When creating configuring a new project for use with this distribution of emacs create a `.dir-local.el` file in the toplevel folder with the following content :
```
((nil . ((cmake-ide-build-dir . "/path/to/build/dir") (cmake-ide-project-dir . "/path/to/project/dir"))))
```

with absolute path to your project cmake root and to your build folder.

You may need to export compile commands with cmake for use by the cmake-ide and rtags plugins with the option `-DCMAKE_EXPORT_COMPILE_COMMANDS=1`
